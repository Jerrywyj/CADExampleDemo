﻿// (C) Copyright 2020 by  
//
using System;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;

// This line is not mandatory, but improves loading performances
[assembly: CommandClass(typeof(TextStyle.MyCommands))]

namespace TextStyle
{

  
    public class MyCommands
    {
       
        [CommandMethod("AddNewTextStyle", CommandFlags.Modal)]
        public void MyCommand() // This method can have any name
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                //设置TrueType字体(仿宋体）
                ObjectId styleId = db.AddTextStyle("仿宋体", "simfang.ttf");
                MText txt1 = new MText();
                txt1.Contents = "仿宋体";
                txt1.TextStyleId = styleId;//文字样式
                //设置SHX字体(romans）
                styleId = db.AddTextStyle("罗马字体", "romans", "gbcbig");
                MText txt2 = new MText();
                txt2.Contents = "罗马字体";
                txt2.TextStyleId = styleId;//文字样式
                txt2.Location = txt1.Location.PolarPoint(Math.PI / 2, 5);
                //设置SHX字体(romans）
                styleId = db.AddTextStyle("垂直罗马字体", "romans", "gbcbig");
                //设置文字样式的各种属性
                styleId.SetTextStyleProp(2.0, 0.67, 15 * Math.PI / 180, true, true, true, AnnotativeStates.True, true);
                MText txt3 = new MText();
                txt3.Contents = "垂直罗马字体";
                txt3.TextStyleId = styleId;
                txt3.Location = txt1.Location.PolarPoint(0, 20);
                //设置TrueType字体(宋体）并且有加粗、倾斜效果
                styleId = db.AddTextStyle("加粗斜宋体", "宋体", true, true, 0, 0);
                MText txt4 = new MText();
                txt4.Contents = "加粗斜宋体";
                txt4.TextStyleId = styleId;
                txt4.Location = txt2.Location.PolarPoint(Math.PI / 2, 5);
                db.AddToModelSpace(txt1, txt2, txt3, txt4);
                txt3.SetFromTextStyle();//必须将文本加入到数据库后再匹配文字样式的属性                
                trans.Commit();
            }
        }

        
    }

}
