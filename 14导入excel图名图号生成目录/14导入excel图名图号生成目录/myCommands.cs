﻿// (C) Copyright 2020 by  yuzhaokai
//
using System;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;


[assembly: CommandClass(typeof(_14导入excel图名图号生成目录.MyCommands))]

namespace _14导入excel图名图号生成目录
{

  
    public class MyCommands
    {

        [CommandMethod("CCA", CommandFlags.Modal)]


        public void CreateCatalogue() // This method can have any name
        {
            Form1 myfrom = new Form1();
            //  Autodesk.AutoCAD.ApplicationServices.Application.ShowModalDialog(myfrom); //在CAD里头显示界面  模态显示

            Autodesk.AutoCAD.ApplicationServices.Application.ShowModelessDialog(myfrom); //在CAD里头显示界面  非模态显示
                                                                                         // myfrom.sh
        }





    }

}
